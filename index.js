const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const {getEvents, dbrequest} = require('./db/eventsdb.js');
const app = express();
// Serve static files from the React app
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'src/client')));

app.post('/events', function (req, res) {
  console.log(req.body);
  getEvents(req.body.topLeft,req.body.bottomRight,pullApi=true)
  .then((result)=>{
    console.log('no. results:', result.events.length);
    res.send(result);
  });
});

const port = process.env.PORT || 5000;
dbrequest.then(() => {
 app.listen(port);
});
