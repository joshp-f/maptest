import React from 'react';
import {render} from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import AppContainer from './framework/containers/AppContainer.js';
import reducer from './framework/reducers';

const store = createStore(
  reducer,
  applyMiddleware(
    thunk,
    logger
  )
);

render(
  <Provider store={store}>
    <AppContainer />
  </Provider>,
  document.getElementById('app')
);
