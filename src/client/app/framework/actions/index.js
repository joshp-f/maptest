
export const setZoom = (zoom) => ({
  type: 'SET_ZOOM',
  zoom: zoom
})

export const setTargetID = (id) => ({
  type: 'SET_EVENT_SELECTION',
  id: id
})

export const setMapCentre = (centre) => ({
  type: 'SET_CENTRE',
  centre: centre
})
export const setUserLocation = (centre) => ({
  type: 'SET_LOCATION',
  centre: centre
})

export const toggleInfoPanel = () => ({
  type: 'TOGGLE_INFO_PANEL'
})


export const setInitialLocation = () => {
  return (dispatch) => {
    if (navigator && navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((pos) => {
        const result = {lat: pos.coords.latitude, lng: pos.coords.longitude};
        dispatch(setMapCentre(result));
        dispatch(setUserLocation(result));
      });
    }
  }
}

export const setBounds = (topLeft, bottomRight) => {
  return {
    type: 'SET_BOUNDS',
    topLeft,
    bottomRight
  }
}

export const changeEventSelection = (id) => { // change target id and move map to centre on event
  return (dispatch, getState) => {
    dispatch(setTargetID(id));
    const eventDict = getState().eventData.dict;
    if (id in eventDict) {
      dispatch(setMapCentre(eventDict[id].position))
    }
  }
}

const signalFetching = (message) => ({
  type: 'FETCHING_STATE',
  message
})

const loadNewEvents = (events, eventSelection) => {
  return (dispatch) => {
    let eventDict = events.reduce(function(map, obj) {
      map[obj.id] = obj;
      return map;
    }, {});
    if (!(eventSelection in eventDict)) {
      dispatch(changeEventSelection(null));
    }
    dispatch(setEvents(events, eventDict));
  }
}

const setEvents = (eventList, eventDict) => ({
  type: 'REPLACE_EVENTS',
  eventDict,
  eventList
})


export const fetchEvents = () => {
  return (dispatch, getState) => {
    const { mapBounds, eventSelection} = getState();
    dispatch(signalFetching('Fetching events...'));
    fetch('https://tranquil-grand-teton-34530.herokuapp.com/events', {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        topLeft: mapBounds.topLeft,
        bottomRight: mapBounds.bottomRight,
      }),
    }).then((response) =>{
      dispatch(signalFetching('Successfully fetched data.'));
      return response.json();
    }).then((info) => {
      console.log(info)
      dispatch(loadNewEvents(info.events, eventSelection));
      if (info.repoll) console.log('repoll true');
    }).catch((err) =>{
      dispatch(signalFetching('Failed to fetch data :('));
      console.error(err);
      console.log('API not usable');
      dispatch(loadNewEvents({"0":{position:{lat:0,lng:0},id:"0",name:'test',link:'www.klsdjxcsk.com',description:'this is a test event',score:27}}));
    });
  }
}
