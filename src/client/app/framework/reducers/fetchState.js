
const fetchState = (state = '', action) => {
  switch (action.type) {
    case 'FETCHING_STATE':
      return action.message
    default:
      return state
  }
}
export default fetchState;
