const infoPanelOpen = (state = true, action) => {
  switch (action.type) {
    case 'TOGGLE_INFO_PANEL':
      return !state
    default:
      return state
  }
}
export default infoPanelOpen;
