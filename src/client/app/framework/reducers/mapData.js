export const zoom = (state = 11, action) => {
  switch (action.type) {
    case 'SET_ZOOM':
      return action.zoom
    default:
      return state
  }
}
export const mapCentre = (state = {lat: -33.920204162597656, lng: 151.1554412841797}, action) => {
  switch (action.type) {
    case 'SET_CENTRE':
      return Object.assign({}, action.centre)
    default:
      return state
  }
}
export const userLocation = (state = {lat: -33.920204162597656, lng: 151.1554412841797}, action) => {
  switch (action.type) {
    case 'SET_LOCATION':
      return Object.assign({}, action.centre)
    default:
      return state
  }
}
export const mapBounds = (state = {topLeft:{lat:0,lng:0},bottomRight:{lat:0,lng:0}}, action) => {
  switch (action.type) {
    case 'SET_BOUNDS':
      return {
        topLeft:Object.assign({}, action.topLeft),
        bottomRight:Object.assign({}, action.bottomRight),
      }
    default:
      return state
  }
}
