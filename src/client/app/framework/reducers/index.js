import { combineReducers } from 'redux'
import {mapCentre, zoom, userLocation, mapBounds} from './mapData.js'
import fetchState from './fetchState'
import eventSelection from './eventSelection'
import eventData from './eventData'
import infoPanelOpen from './infoPanelOpen'
const eventViewerApp = combineReducers({
  eventSelection,
  eventData,
  mapCentre,
  zoom,
  userLocation,
  mapBounds,
  fetchState,
  infoPanelOpen,
})

export default eventViewerApp
