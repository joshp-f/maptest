const eventSelection = (state = null, action) => {
  switch (action.type) {
    case 'SET_EVENT_SELECTION':
      return action.id
    default:
      return state
  }
}
export default eventSelection;
