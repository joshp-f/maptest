const eventData = (state = {dict:{}, array:[]}, action) => {
  switch (action.type) {
    case 'REPLACE_EVENTS':
      return {dict: action.eventDict, array: action.eventList};
    default:
      return state
  }
}
export default eventData;
