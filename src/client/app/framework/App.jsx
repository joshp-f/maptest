import React from 'react';
import PropTypes from 'prop-types';
import MapContainer from './containers/MapContainer.js';
import InfoWindowContainer from './containers/InfoWindowContainer.js';
import SearchBarContainer from './containers/SearchBarContainer.js';
import VisibleEventList from './containers/VisibleEventList.js';
import './app.css';
class App extends React.Component {

  constructor(props) {
    super(props);
  }
  componentDidMount() {
    this.props.setInitialLocation();
  }
  render() {
    return (
      <div>
        <div className='map-holder'>
          { this.props.eventSelection && <InfoWindowContainer /> }
          <MapContainer />

        </div>
        <div className='eventlist-holder'>
          <SearchBarContainer/>
          <VisibleEventList/>
        </div>
      </div>
    );
  }

}

App.propTypes = {
  eventSelection: PropTypes.string,
  setInitialLocation: PropTypes.func
}
export default App;
