import { connect } from 'react-redux';
import { changeEventSelection , setZoom} from '../actions';
import EventList from '../components/Feed/EventList.jsx';

const mapStateToProps = (state) => ({
  eventData: state.eventData.array,
  eventSelection: state.eventSelection
})

const mapDispatchToProps = (dispatch) => {
  return {
    onEventClick: (id) => {
      dispatch(changeEventSelection(id));
      dispatch(setZoom(10));
    }
  }
}

const VisibleEventList = connect(
  mapStateToProps,
  mapDispatchToProps
)(EventList)

export default VisibleEventList;
