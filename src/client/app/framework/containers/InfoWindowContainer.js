import { connect } from 'react-redux';
import { changeEventSelection } from '../actions';
import InfoWindow from '../components/InfoWindow/InfoWindow.jsx';

const mapStateToProps = (state) => ({
  data: state.eventData.dict[state.eventSelection],
  userLocation: state.userLocation,
  mapCentre: state.mapCentre,
})

const mapDispatchToProps = {
  upvote: changeEventSelection
}

const InfoWindowContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(InfoWindow)

export default InfoWindowContainer;
