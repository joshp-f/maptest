import { connect } from 'react-redux';
import { fetchEvents, toggleInfoPanel } from '../actions';
import SearchBar from '../components/SearchBar/SearchBar.jsx';

const mapStateToProps = (state) => ({
  fetchMessage: state.fetchState,
  infoPanelOpen: state.infoPanelOpen,
  //eventSelection: state.eventSelection
})

const mapDispatchToProps = (dispatch) => {
  return {
    fetchData: () => dispatch(fetchEvents()),
    toggleInfoPanel: () => dispatch(toggleInfoPanel())
  }
}

const SearchBarContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchBar)

export default SearchBarContainer;
