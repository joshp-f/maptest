
import { connect } from 'react-redux';
import { setInitialLocation } from '../actions';
import App from '../App.jsx';

const mapDispatchToProps = (dispatch) => {
  return {
    setInitialLocation: () =>  dispatch(setInitialLocation())
  }
}
const mapStateToProps = (state) => ({
  eventSelection: state.eventSelection,
})
const AppContainer = connect(mapStateToProps, mapDispatchToProps)(App);

export default AppContainer;
