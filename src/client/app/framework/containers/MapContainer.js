import React from 'react';
import {GoogleApiWrapper} from 'google-maps-react';
import { connect } from 'react-redux'
import { fetchEvents, setMapCentre, setZoom, setTargetID, setBounds} from '../actions';
import PropTypes from 'prop-types';
import Map from '../components/Map/Map.jsx';

const mapStateToProps = (state) => {
  return {
    eventData: state.eventData.array,
    zoom: state.zoom,
    mapCentre: state.mapCentre
}}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchData: () => dispatch(fetchEvents()),
    changeCentre: centre => dispatch(setMapCentre(centre)),
    changeZoom: zoom => dispatch(setZoom(zoom)),
    changeTarget: id => dispatch(setTargetID(id)),
    setBounds: (topLeft, bottomRight) => dispatch(setBounds(topLeft, bottomRight))
  }
}

const reduxWrapper = connect(mapStateToProps, mapDispatchToProps)(Map);


export default GoogleApiWrapper({
  version: '3.29',
  apiKey: 'AIzaSyAYQVR5K1zkpCn4dieDdoyKudCH9snsdK0',
  libraries: ['places'],
})(reduxWrapper);
