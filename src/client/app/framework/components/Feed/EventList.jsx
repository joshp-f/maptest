import React from 'react';
import PropTypes from 'prop-types';
import EventBar from './EventBar.jsx';
import './eventList.css';
export class EventList extends React.Component {
  constructor(props){
    super(props);
  }
  render(){
    return (
      <div className='list-align'>
        {this.props.eventData.map(({name, id, link, description, score}) =>
          <EventBar key={id}
            name={name}
            id={id}
            link={link}
            description={description.substring(0,30)+'...'}
            score={score}
            onClick={this.props.onEventClick}
            highlight={id == this.props.eventSelection}
          />
        )}
      </div>
    );
  }
}

EventList.propTypes = {
  onEventClick: PropTypes.func,
  eventData: PropTypes.array,
  eventSelection: PropTypes.string,
};

export default EventList;
