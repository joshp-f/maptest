import React from 'react';
import PropTypes from 'prop-types';
import './eventBar.css';
export class EventBar extends React.Component {
  render(){
    return (
      <div
      className={'outer' + (this.props.highlight ? ' outer-selected' : '')}
      onClick={() => this.props.onClick(this.props.id)}>
        <p className='score'> {this.props.score} </p>
        <div className='inner' >

          <a className='title' href={this.props.link}>{this.props.name}</a>
          <p className='description'>{this.props.description}</p>
        </div>
      </div>
    );
  }
}

EventBar.propTypes = {
  name: PropTypes.string,
  id:PropTypes.string,
  link:PropTypes.string,
  description:PropTypes.string,
  score: PropTypes.number,
  onClick: PropTypes.func,
  highlight: PropTypes.bool,
};
export default EventBar;
