import React from 'react';
import PropTypes from 'prop-types';
import './infoWindow.css';
class InfoWindow extends React.Component {

  constructor(props) {
    super(props);
  }

  render() { // TODO ALSO INCLUDE A BUTTON TO FIND ROUTE, COMMENTS
    const info = this.props.data;
    const routeURL = 'https://www.google.com/maps/dir/?api=1&destination='+info.position.lat+','+info.position.lng;
    const date = new Date(info.start_time);
    return (
      <div className='window'>
        <div className='heading'>
          <div className='lhs'>
          <div className='score'>
            {info.score}
          </div>
          <a className='title' href={info.link}>
            {info.name}
          </a>
          </div>
          <a className='findRoute' href={routeURL}>
            Find route
          </a>
        </div>
        <div className='body'>
          <div className='time'>
            {date.toDateString()}
          </div>
          <div className='description'>
            {info.description}
          </div>
        </div>
      </div>
    );
  }

}

InfoWindow.propTypes = {
  data: PropTypes.object,
  upvote: PropTypes.func,
  userLocation: PropTypes.object,
  mapCentre: PropTypes.object,
}
export default InfoWindow;
