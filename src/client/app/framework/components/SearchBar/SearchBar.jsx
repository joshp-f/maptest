import React from 'react';
import PropTypes from 'prop-types';
import InfoPanel from './InfoPanel.jsx';
import './searchBar.css';
export class SearchBar extends React.Component {
  render(){
    return (
      <div>
        <div className='searchRow'>
          <div className='infoButton' onClick={this.props.toggleInfoPanel}>
             info
          </div>
          <div className='searchButton' onClick={this.props.fetchData}>
            Search Events
          </div>
        </div>
        { this.props.infoPanelOpen && <InfoPanel /> }
        <div className='fetchState'>
        {this.props.fetchMessage}
        </div>
      </div>
    );
  }
}

SearchBar.propTypes = {
  fetchMessage: PropTypes.string,
  infoPanelOpen: PropTypes.bool,
  fetchData: PropTypes.func,
  toggleInfoPanel: PropTypes.func,
};
export default SearchBar;
