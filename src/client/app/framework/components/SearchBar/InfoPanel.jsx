import React from 'react';
import './infoPanel.css';
export const InfoPanel = () =>
   (
    <div className='infoPanel'>
      <div>
        This site ranks meetup.com events by number of attendees and displays them on a map.<br/>
        I am using a dynamically updating quadtree with mongodb to only show relevant events within the users viewport.<br />
        <p/>
        Why are events out of date?<br/>
        I have turned off automatic event fetching and insertion. 
        <p/>
        Details:
        <br/>
        <a href={'https://www.linkedin.com/in/joshua-porritt-fraser'}>
          linkedin
        </a>
        <br/>
        <a href={'https://github.com/joshp-f'}>
          github
        </a>
        <p/>
        Close this window by clicking the button labelled 'info' above

      </div>
    </div>
  )
export default InfoPanel;
