import React from 'react';
import PropTypes from 'prop-types';

export class Marker extends React.Component {
  // componentDidUpdate(prevProps) {
  //   if ((this.props.map !== prevProps.map) ||
  //   (this.props.position !== prevProps.position)) {
  //     this.renderMarker();
  //     console.log('here');
  //   }
  // }
  componentDidMount(){
    this.renderMarker();
  }
  componentWillUnmount() {
    if (this.marker) {
      this.marker.setMap(null);
    }
    else{
      console.log('failed to remove marker due to marker missing');
    }
  }
  renderMarker = () => {
    let {
      map, google, position, score
    } = this.props;
    let pos = position;

    position = new google.maps.LatLng(pos.lat, pos.lng);
    const pref = {
      map: map,
      position: position,
      label: score.toString()
    };
    this.marker = new google.maps.Marker(pref);
    //const evtNames = ['click', 'mouseover'];
    this.marker.addListener('mouseover', () => {
    });
    this.marker.addListener('click', (evt) => {
      this.props.onClick();
    });
  }
  render() {
    return null;
  }
}

Marker.propTypes = {
  google: PropTypes.object,
  map: PropTypes.object,
  onClick: PropTypes.func,
  position: PropTypes.object,
  id: PropTypes.string,
  score: PropTypes.number,
};

export default Marker;
