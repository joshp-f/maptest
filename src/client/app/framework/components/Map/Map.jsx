// import ReactDOM from 'react-dom';
import React from 'react';
import { connect } from 'react-redux'
import PropTypes from 'prop-types';
import Marker from './Marker.jsx';
class Map extends React.Component {
  constructor(props) {
    super(props);
  }

  requestEvents = () => {
    if (this.map) {
      this.props.fetchData();
    }
  }
  changeCentre = () => {
    const centre = this.map.getCenter().toJSON();
    this.props.changeCentre(centre);
  }
  moveMap = () => {
    if (this.map) {
      const {lat, lng} = this.props.mapCentre;
      let center = new this.props.google.maps.LatLng(lat, lng);
      this.map.panTo(center);
    }
  }
  zoomMap = () => {
    if (this.map) {
      this.map.setZoom(this.props.zoom);
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.google !== this.props.google) {
      this.loadMap();
    }
    if (prevProps.mapCentre !== this.props.mapCentre) {
      this.moveMap();
    }
    if(prevProps.zoom !== this.props.zoom) {
      this.zoomMap();
    }
  }

  componentDidMount() {
    this.loadMap();
  }

  loadMap() {
    if (this.props && this.props.google) {
      const {google} = this.props;
      const maps = google.maps;
      const {lat, lng} = this.props.mapCentre;
      const centre = new maps.LatLng(lat, lng);
      const mapConfig = Object.assign({}, {
        center: centre,
        zoom: this.props.zoom
      });
      this.map = new maps.Map(this.node, mapConfig);

      let autocomplete = new google.maps.places.Autocomplete(this.searchboxnode);
      autocomplete.bindTo('bounds', this.map);
      this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(this.searchboxnode);
      autocomplete.addListener('place_changed', () => {
        let place = autocomplete.getPlace();
        if (!place.geometry) {
          return;
        }

        if (place.geometry.viewport) {
          this.map.fitBounds(place.geometry.viewport);
        } else {
          this.map.setCenter(place.geometry.location);
        }
      });
      let mapLoaded;
      this.map.addListener('tilesloaded', () => {
        if (!mapLoaded) {
          this.requestEvents();
          mapLoaded = true;
        }
      });
      let centerChangedTimeout;
      this.map.addListener('dragend', () => {
        if (centerChangedTimeout) {
          clearTimeout(centerChangedTimeout);
          centerChangedTimeout = null;
        }
        centerChangedTimeout = setTimeout(() => {
          this.changeCentre();
          // this.requestEvents();
        }, 1.5);
      });
      this.map.addListener('click', () => {
        this.props.changeTarget(null)
      });
      this.map.addListener('zoom_changed', () => {
        const zoomLevel = this.map.getZoom();
        this.props.changeZoom(zoomLevel)
      });
      this.map.addListener('bounds_changed', () => {
        let bounds = this.map.getBounds();
        const NE = bounds.getNorthEast();
        const SW = bounds.getSouthWest();
        this.props.setBounds({lat: NE.lat(),lng: NE.lng()}, {lat: SW.lat(), lng: SW.lng()});
      });
    }
  }

  render() {
    const renderMarkers = () => {
      return this.props.eventData.map(({id, position, score}) => {
        return <Marker
          key={id}
          id={id}
          position={position}
          score={score}
          google={this.props.google}
          map={this.map}
          onClick={() => this.props.changeTarget(id)}
      />
    })
    }
    const style = {
      width: '100%',
      height: '100%',
    };
    return (
      <div>
        { this.map && renderMarkers()}
        <input ref={node => this.searchboxnode = node} type="text"/>
        <div ref={node => this.node = node} style={style}>
          Loading map...
        </div>
      </div>
    );
  }
}

Map.propTypes = {
  google: PropTypes.object,
  eventData: PropTypes.array,
  zoom: PropTypes.number,
  mapCentre: PropTypes.object,
  changeZoom: PropTypes.func,
  changeCentre: PropTypes.func,
  changeTarget: PropTypes.func,
  fetchData: PropTypes.func,
  setBounda: PropTypes.func
};

export default Map;
