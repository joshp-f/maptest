"# API documentation"

"##/events"

post

returns dictionary mapping from unique id to:

position: has float lat and lon fields

id: unique id

name: event name

link: link to page of events

description: text description of event
