const {quadS, kmPerDegree} = require('./constants.js');
const rankEvents = (eventCollection, updatePositions, eventPositions, eventRankings) => {
  const computeTopEvents = (_id, depth, centre) => {
    const range = Math.pow(2,depth)*quadS/kmPerDegree;
    return isUpdateNeeded(centre, range)
    .then(needed => {
      if (!needed) { // no update needed, just return list
        return eventRankings.findOne({ _id })
        .then(result => {
          return result.events;
        })
      }
      else {
        if (depth <= 0) {
          return findEvents(eventPositions, centre, range).limit(10).toArray()
          .then((topRanked) => {
            const ids = topRanked.map(ev => {return ev.id});
            return eventCollection.find({id:{'$in':ids}},{id:1, score:1}).toArray()
          })
          .then(final => {
            return eventRankings.update(
              {_id},
              {$set:{events:final}}
            )
            .then( () => {
              return final
            })
          })
        }
        else {
          return eventRankings.findOne({ _id })
          .then(result => {
            let promises = [];
            const args = getSubQuads(centre, range);
            for (let i = 0;i<4; i++) {
              let insertAction;
              if (!result.children[i]) {
                const myTemplate = template(depth-1, args[i]);
                insertAction = eventRankings.insertOne(myTemplate);
                result.children[i] = myTemplate._id; //TODO CHECK THAT THIS ASSIGNS CORRECTLY
              }
              else insertAction = Promise.resolve();
              promises.push(
                insertAction.then(() => {
                  return computeTopEvents(result.children[i],depth-1,args[i]);
                })
              );
            }
            return Promise.all(promises)
            .then(eventsArr => {
              let final = [];
              eventsArr.forEach(arr => {
                final = final.concat(arr);
              })

              if(depth == 13) {
                 console.log(final)
              }
              final.sort((a,b) => {
                return (b.score-a.score); //sorts so larger score is first
              })
              final =  final.splice(0, 10); // limits to top 10

              eventRankings.update(
                { _id },
                {$set:
                  {
                    children: result.children,
                    events: final
                  }
                }
              )
              return final;
            })
          });
        }
      }
    })
  }



  const initTop = () => {
    return eventRankings.findOne({depth:13})
    .then(result => {
      if (!result) {
        const myTemplate = template(13, {lat:0,lng:0});
        return eventRankings.insert(myTemplate)
        .then( () => {
          return myTemplate._id;
        });

      }
      else {
        return result._id;
      }
    })
  }


  const isUpdateNeeded = (centre,range) => {
    return findEvents(updatePositions, centre, range).limit(1).count()
    .then( found => {
      return Boolean(found);
    })
  }
  const template = (depth, location) => {
    return {
      depth:depth,
      events:[],
      children:[null,null,null,null],
      position:{
        type:"Point",
        coordinates:[location.lng, location.lat]
      }
    }
  }
  const findEvents = (collection, centre, range ) => {
    return collection.find({ //find if some event in quad
      position:{
        '$geoWithin':{
          '$box': [[centre.lng-range, centre.lat-range],[centre.lng+range,centre.lat+range]]
        }
      }
    })
  }


  const getSubQuads = (centre, range) => {
    const disp = range/2;
    return [{lat:centre.lat-disp/2, lng:centre.lng-disp},
      {lat:centre.lat+disp/2, lng:centre.lng-disp},
      {lat:centre.lat-disp/2, lng:centre.lng+disp},
      {lat:centre.lat+disp/2, lng:centre.lng+disp},
    ]
  }
  return initTop().then(_id => {
     return computeTopEvents(_id,13,{lat:0,lng:0});
  })
}

module.exports = {rankEvents};
