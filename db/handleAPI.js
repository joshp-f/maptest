var fetch = require('node-fetch');

const requestMeetupEvents = (loc) => {
  const url = 'https://api.meetup.com/find/upcoming_events?photo-host=public&sig_id=238053391' +
   '&page=' + '100' +
   '&radius=' + 'smart' +
   '&lon=' + loc.lng +
   '&lat=' + loc.lat +
   '&key=' + '38156f3d1f7f437722574d283a135214' +
   '&fields=' + 'plain_text_description'
   console.log(url);
  return fetch(url)
  .then((res) => {
    return res.json();
  }).then((myjson) => {
    console.log(Object.keys(myjson))
    if(myjson.errors) {
      console.log('meetup errors', myjson.errors)
    }
    if(!(myjson.events && myjson.city)) {
      console.log('meetup request wrong');
      return {events:[], city: null}
    }
    else console.log("Meetup Request sent")
    return myjson;
  })
  .catch(err => {
    console.error('meetup request error', err);
  })
}

const storeNewMeetupEvents = (loc, sourceMeetupData, eventCollection, eventPositions, updatePositions) => {
  return requestMeetupEvents(loc)
  .then ( (result) => {
    sourceMeetupData.insert(result);
    const generalised = result.events.map(ev => {
      return generaliseMeetupEvent(ev, result.city);
    });
    let promises = upsertMany(eventCollection, generalised);
    const positionData = generalised.map(ev => {
      return {
        id:ev.id,
        position:{
          type:"Point",
          coordinates:[ev.position.lng, ev.position.lat]
        }
      }
    })
    promises = promises.concat(upsertMany(eventPositions, positionData));
    console.log('inserted events');
    promises = promises.concat(upsertMany(updatePositions, positionData));
    return Promise.all(promises);
  });
}

const upsertMany = (collection, events) => {
  return events.map((ev) => {
    collection.updateOne(
      {
        id:ev.id
      },
      ev,
      {
        upsert:true
      }
    );
  });
}

const generaliseMeetupEvent = (event, city) => {
  const position = (event.venue && event.venue.lat) ? {lat:event.venue.lat, lng: event.venue.lon} : (
    (event.group && event.group.lon) ? {lat:event.group.lat, lng: event.group.lon} : (
      (city && city.lon) ? {lat:city.lat, lng: city.lon} : {lat:0, lng:0}
    )
  );
  return {
    start_time: event.time,
    description: (event.plain_text_description || ''),
    duration: (event.duration || 10800000),
    group_name: event.group.name,
    id: event.id,
    link: event.link,
    name: event.name,
    score: event.yes_rsvp_count,
    position,
    source: 'meetup'
  }
}

module.exports = storeNewMeetupEvents;
