const deleteRankings = (eventRankings) => {
  return eventRankings.remove();
}


const randomizeScore = (eventPositions) => {
  eventPositions.find().forEach((doc) => {
    eventPositions.update({_id:doc._id},{$set:{score:Math.floor(Math.random()*100)}})
  })
}

const getCentre = (topLeft, bottomRight) => {
  return {
    lat:(topLeft.lat+bottomRight.lat)/2,
    lng:(topLeft.lng+bottomRight.lng)/2
  }
};

const deleteDuplicates = (eventPositions, eventCollection) => {
  eventPositions.aggregate([{$group:{_id:"$id", dups:{$push:"$_id"}, count: {$sum: 1}}},
  {$match:{count: {$gt: 1}}}
  ]).forEach(function(doc){
    doc.dups.shift();
    eventPositions.remove({_id : {$in: doc.dups}});
  });
  eventCollection.aggregate([{$group:{_id:"$id", dups:{$push:"$_id"}, count: {$sum: 1}}},
  {$match:{count: {$gt: 1}}}
  ]).forEach(function(doc){
    doc.dups.shift();
    eventCollection.remove({_id : {$in: doc.dups}});
  });
}

const idToEvents = (ids, collection) => {
  return collection.find({
    id:{
      '$in': ids
    }
  }).toArray();
}

const signalAll = (eventPositions, updatePositions) => {
  let promises = [];
  eventPositions.find().forEach((ev) =>{
    delete ev._id;
    promises.push(updatePositions.insert(ev));
  })
  return Promise.all(promises);
}

const signalUpdate = (centre, updatePositions) => {
  return updatePositions.insert({
    position:{
      type:"Point",
      coordinates:[centre.lng, centre.lat]
    }
  });
}

const newRequestNeeded = (pos, requestPositions) => {
  return requestPositions.find({
    position:{
      '$near':{
        '$geometry': {
          type: 'Point',
          coordinates: [pos.lng, pos.lat]
        },
        '$maxDistance':4000,
      }
    }
  }).limit(1).count(); // TODO WIERD BUG WHERE THIS PRODUCES 2 ???
}

const insertNewRequest = (pos, requestPositions) => {
  requestPositions.insert({
    position:{
      type:"Point",
      coordinates:[pos.lng, pos.lat]
    }
  });
}
module.exports = {
  deleteRankings, randomizeScore, getCentre,
  deleteDuplicates, idToEvents, signalAll,
  signalUpdate, newRequestNeeded, insertNewRequest
}
