const constants = {};
constants.kmPerDegree = 111.32; // kilometres per degree of longitude at equator
// since 180 = 2^13*quadS/111.32, quadS = 180/2^13*111.32
constants.quadS = 180/Math.pow(2,13)*constants.kmPerDegree;
module.exports = constants;
