const MongoClient = require('mongodb').MongoClient;
const {quadS, kmPerDegree} = require('./constants.js');
const { rankEvents } = require('./rankingQuadTree.js');
const storeNewMeetupEvents = require('./handleAPI.js');
const {
  deleteRankings, randomizeScore, getCentre,
  deleteDuplicates, idToEvents, signalAll,
  signalUpdate, newRequestNeeded, insertNewRequest} = require('./helpers.js');
// const url = "mongodb://localhost:27017/eventdb";
const url = "mongodb://joshp-f:p0AiT2sZAY82@ds259855.mlab.com:59855/eventviewer"
const dbrequest = MongoClient.connect(url);
let db, eventPositions, eventCollection, requestPositions, updatePositions, eventRankings;

dbrequest.then((ndb) => {
  db = ndb;
  sourceMeetupData = db.collection('sourceMeetupData');
  eventCollection = db.collection('eventCollection');
  eventPositions = db.collection('eventPositions');
  requestPositions = db.collection('requestPositions');
  updatePositions = db.collection('updatePositions');
  eventRankings = db.collection('eventRankings'); // TODO change this back
  initIndexes();

})

let lastUpdate = Date.now();

const initIndexes = () => {
  eventRankings.createIndex({depth:1,position:"2dsphere"})
  requestPositions.createIndex({position:"2dsphere"})
  updatePositions.createIndex({position:"2dsphere"})
  eventPositions.createIndex({depth:1,position:"2dsphere"})
  eventCollection.createIndex({id:1})
}

const getEvents = (topLeft,bottomRight, pullApi=false) => {
  const centre = getCentre(topLeft, bottomRight);
  // signalAll(eventPositions, updatePositions);
  return newRequestNeeded(centre, requestPositions)
  .then(closeRequest =>{
    if(false){//if(!closeRequest) {
      insertNewRequest(centre, requestPositions)
      return storeNewMeetupEvents(centre, sourceMeetupData, eventCollection, eventPositions, updatePositions)
      .then ( () => {
        return rankEvents(eventCollection, updatePositions, eventPositions, eventRankings);
      })
      .then(() => {
        deleteRankings(updatePositions);
      })
      .then(() => {
        return getTopEvents(topLeft, bottomRight);
      })
      .then ( (events) => {
        return {repoll: true, events};
      })
    }
    else return getTopEvents(topLeft, bottomRight)
    .then ((events) => {
      return {repoll: false, events};
    })
  })
};

const resetDB = () => {
  console.log('removing all db')
  sourceMeetupData.remove({})
  eventCollection.remove({})
  eventPositions.remove({})
  requestPositions.remove({})
  updatePositions.remove({})
  eventRankings.remove({})
}

const getDatabaseEvents = (topLeft, bottomRight) => {
  return eventPositions.find({
    position:{
      $geoWithin:{
        $box:[[topLeft.lng,topLeft.lat],[bottomRight.lng, bottomRight.lat]]
      }
    }
  }).toArray()
  .then(result => {
    ids = result.map(dict => dict.id);
    return idToEvents(ids, eventCollection);
  });
}


const getTopEvents = (topLeft, bottomRight) => {
  const range = Math.abs(topLeft.lng-bottomRight.lng);
  const numSegments = range*kmPerDegree/quadS; //the number of 4km quads on screen width
  const depth = Math.min(13, Math.floor(Math.max(Math.log2(numSegments)-1,0))); // depth 0 represents pulling 2^0*minQuadSized sized boxes
  console.log('depth' ,depth);
  return grabQuadEvents(getCentre(topLeft, bottomRight), range, depth)
}

const grabQuadEvents = (centre, range,depth) => {
  console.log('centre', centre, 'range' ,range)
  return eventRankings.find({
    depth,
    position:{
      '$geoWithin':{
        '$box': [[centre.lng-range,centre.lat-range],[centre.lng+range,centre.lat+range]]
      }
    }
  }).toArray()
  .then(result =>{
    let events = [];
    result.forEach( elem => {
      events = events.concat(elem.events);
    })

    events = events.map (ev => {return ev.id});
    return idToEvents(events, eventCollection)
    .then(events => {
      events.sort((a,b) => b.score-a.score);
      return events;
    });
  })
}

module.exports = {getEvents, dbrequest};
