var fetch = require('node-fetch');
const key = '38156f3d1f7f437722574d283a135214';

const reformatEventData = (ev) => {
  let template = {
    id: 'uh oh work out why this field is undefined',
    position: {lat:0, lng: 0},
    link: '',
    name: '',
    description: '',
  }
  let target;
  if ('venue' in ev) target = 'venue';
  else if ('group' in ev) target = 'group';
  if (target) template.position = {
    lat: ev[target].lat,
    lng: ev[target].lon,
  };
  const toCopy = ['id', 'link', 'name','description'];
  toCopy.forEach((key) => {
    if (key in ev) template[key] = ev[key];
    if (!('id' in ev)) console.log("OHSHIT ID NOT FOUND");
  });
  template.score = Math.floor(Math.random()*100)
  template.description = template.description.replace(/<.*?>/g, '');
  return template;

}
// 'https://api.meetup.com/find/events?&sign=true&photo-host=public&lon=151.209296&lat=-33.868820&key=
const grabMeetupEvents = (loc) => {
  const url = 'https://api.meetup.com/find/events?&sign=true&photo-host=public'
  + '&only=description,link,group,venue,id,name' + '&radius=' + '3' + '&lon=' + loc.lng + '&lat=' + loc.lat + '&key=' + key;// 3 miles
  return fetch(url)
  .then((res) => {
    return res.json();
  }).then((myjson) => {
    if (!(myjson === {} || Array.isArray(myjson))) {
      console.log('meetup request failed' + myjson);
      return [];
    }
    let final = [];
    myjson.forEach((ev) =>{
      processed = reformatEventData(ev);
      final.push(processed);
    });
    console.log("Meetup Request sent")
    return final;
  });
}

module.exports = grabMeetupEvents;
