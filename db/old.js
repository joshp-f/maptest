
const computeTopEvents = (depth=13, centre={lat:0,lng:0}) => {
  const range = Math.pow(2,depth)/degreeToQuads;
  const getSubQuads = (centre, range) => {
    const disp = range/2;
    return [{lat:centre.lat-disp, lng:centre.lng-disp},
      {lat:centre.lat+disp, lng:centre.lng-disp},
      {lat:centre.lat-disp, lng:centre.lng+disp},
      {lat:centre.lat+disp, lng:centre.lng+disp},
    ]
  }
  return updatePositions.find({ //find if some event in quad
    position:{
      '$geoWithin':{
        '$box': [[centre.lng-range, centre.lat-range],[centre.lng+range,centre.lat+range]]
      }
    }
  }).limit(1).count()
  .then( found => {
    if (found) {
      if (depth > 0) {
        const childQuads = getSubQuads(centre, range);
        const promises = []
        childQuads.forEach((newCentre) => {
          promises.push(computeTopEvents(depth-1, {lat:newCentre.lat,lng:newCentre.lng}));
        })
        return Promise.all(promises) // ensures function has terminated for deeper nodes
        .then(() => {
          if (depth == 13){ //post condition stating all updates have been processed
            updatePositions.remove({ //find if some event in quad
              position:{
                '$geoWithin':{
                  '$box': [[centre.lng-range, centre.lat-range],[centre.lng+range,centre.lat+range]]
                }
              }
            })
          }
          return eventRankings.find({
            depth:depth-1,
            position:{
              '$geoWithin':{
                '$box': [[centre.lng-range, centre.lat-range],[centre.lng+range,centre.lat+range]]
              }
            }
          }).limit(10).toArray()
          .then((topRanked) => {
            let final = [];
            topRanked.forEach(event => {
              event.depth++;
              delete event._id;
              eventRankings.insert(event);
              final.push(event);
            })
          })
        })
      }
      else return eventPositions.find({
        position:{
          '$geoWithin':{
            '$box': [[centre.lng-range, centre.lat-range],[centre.lng+range,centre.lat+range]]
          }
        }
      }).limit(10).toArray()
      .then((topRanked) => {

        let final = [];
        topRanked.forEach(event => {
          event.depth = 0
          delete event._id;
          eventRankings.insert(event);
          final.push(event);
        })
        // eventRankings.insertMany(final);
      })
    }
  })
}
